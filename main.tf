provider "gitlab" {
    token = "${var.gitlab_token}"
}

resource "gitlab_project" "my_project" {
    name = "terraform-created-project"
    description = "my first tf expirience"
    visibility_level = "public"
}

resource "gitlab_deploy_key" "add_deploy_key" {
    project = "${gitlab_project.my_project.id}"
    title = "my example key"
    key = "${var.my_pub_key}"
    can_push = "true"
}